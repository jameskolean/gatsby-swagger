// src/pages/index.js

import * as React from "react"
import Swagger from "../components/swagger";

const IndexPage = () => {
  return (
    <Swagger
      componentId="petstore-api"
      url="https://petstore.swagger.io/v2/swagger.json"
    />
  )
}

export default IndexPage
