// src/components/swagger.js

import React from "react";
import Helmet from "react-helmet";

export default class Swagger extends React.Component {
    componentDidMount() {
        const { componentId, url } = this.props;
        window.SwaggerUI({
            dom_id: `#${componentId}`,
            url: url,
        });
    }
    render() {
        return (
            <>
                <Helmet>
                    // NOTE: I append /gatsby-swagger to support gitlab pages.
                    // I'm sure there is a better way to do this
                    <link href="/gatsby-swagger/swagger/theme-newspaper.css" rel="stylesheet" />
                </Helmet>
                <div id={this.props.componentId} />
            </>
        );
    }
}
