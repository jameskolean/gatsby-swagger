module.exports = {
  siteMetadata: {
    title: `Gatsby Swagger Sample`,
    description: `Sample Gatsby project with Swagger API page.`,
    author: `jameskolean@gmail.com`,
    siteUrl: `https://jameskolean.gitlab.io`,
  },
  pathPrefix: `/gatsby-swagger`,
  plugins: []
}
