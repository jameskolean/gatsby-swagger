// gatsby-browser.js
/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

import SwaggerUI from "swagger-ui";
window.SwaggerUI = SwaggerUI;
